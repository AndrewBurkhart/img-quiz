import React, { useState, useEffect, useCallback } from 'react';
import Question from './Question';
import { loadQuestions } from '../helpers/QuestionsHelper';
import HUD from './HUD';
import SettingsForm from './SettingsForm';
import FinalScore from './FinalScore';

export default function Game({ history }) {
  const [questions, setQuestions] = useState([]);
  const [currentQuestion, setCurrentQuestion] = useState(null);
  const [loading, setLoading] = useState(true);
  const [setting, setSetting] = useState(true);
  const [team1Score, setTeam1Score] = useState(0);
  const [team2Score, setTeam2Score] = useState(0);
  const [team1Bonus, setTeam1Bonus] = useState(0);
  const [team2Bonus, setTeam2Bonus] = useState(0);
  const [questionNumber, setQuestionNumber] = useState(0);
  const [done, setDone] = useState(false);
  const [amount, setAmount] = useState('3');
  const [category, setCategory] = useState('11');
  const [difficulty, setDifficulty] = useState('medium');

  // const scoreSaved = () => {
  //   history.push("/");
  // };

  const settingsSaved = (amount, category, difficulty) => {
    setAmount(amount);
    setCategory(category);
    setDifficulty(difficulty);
    setSetting(false);
    loadQuestions(amount, category, difficulty)
      .then(setQuestions)
      .catch(console.error);
  };

  const changeQuestion = useCallback(
    (team1Bonus = 0, team2Bonus = 0) => {
      if (questions.length === 0) {
        setDone(true);
        setTeam1Score(team1Score + team1Bonus);
        return setTeam2Score(team2Score + team2Bonus);
      }

      // for (let i = 0; i < 4; i++) {
      //   document.getElementById(i).classList.remove('correct');
      // }

      const randomQuestionIndex = Math.floor(Math.random() * questions.length);
      const currentQuestion = questions[randomQuestionIndex];
      const remainingQuestions = [...questions];
      remainingQuestions.splice(randomQuestionIndex, 1);

      setQuestions(remainingQuestions);
      setCurrentQuestion(currentQuestion);
      setLoading(false);
      // console.log('team1Bonus');
      // console.log(team1Bonus);
      // console.log('team2Bonus');
      // console.log(team2Bonus);
      setTeam1Score(team1Score + team1Bonus);
      setTeam2Score(team2Score + team2Bonus);
      setQuestionNumber(questionNumber + 1);
    },
    [
      team1Score,
      team2Score,
      team1Bonus,
      team2Bonus,
      questionNumber,
      questions,
      setQuestions,
      setLoading,
      setCurrentQuestion,
      setQuestionNumber,
    ]
  );

  useEffect(() => {
    if (!currentQuestion && questions.length) {
      changeQuestion();
    }
  }, [currentQuestion, questions, changeQuestion]);

  return (
    <>
      {setting && !done && <SettingsForm settingsSaved={settingsSaved} />}
      {loading && !setting && !done && <div id="loader" />}
      {!done && !loading && !setting && currentQuestion && (
        <div>
          <HUD
            team1Score={team1Score}
            team2Score={team2Score}
            questionNumber={questionNumber}
            totalQuestions={amount}
          />
          <Question
            question={currentQuestion}
            changeQuestion={changeQuestion}
            questionNumber={questionNumber}
            totalQuestions={amount}
          />
        </div>
      )}
      {done && <FinalScore team1Score={team1Score} team2Score={team2Score} />}
    </>
  );
}
