import React from 'react';
import ProgressBar from './ProgressBar';

export default function HUD({
  team1Score,
  team2Score,
  questionNumber,
  totalQuestions,
}) {
  return (
    <div id="hud">
      <div className="hud-item">
        <p className="hud-prefix">
          Question {questionNumber}/{totalQuestions}
        </p>
        <ProgressBar max={totalQuestions} current={questionNumber} />
      </div>
      <div className="hud-item">
        <img src="img.png" width="15%" alt="Red Stripe Logo" />
      </div>
      <div className="hud-item yellowTeam">
        <p className="hud-prefix">Yellow Team</p>
        <h1 className="hud-main-text">{team1Score}</h1>
      </div>
      <div className="hud-item blueTeam">
        <p className="hud-prefix">Blue Team</p>
        <h1 className="hud-main-text">{team2Score}</h1>
      </div>
    </div>
  );
}
