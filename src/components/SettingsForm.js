import React, { useState } from 'react';

export default function SettingsForm({ settingsSaved }) {
  const [amount, setAmount] = useState('10');
  const [category, setCategory] = useState('9');
  const [difficulty, setDifficulty] = useState('medium');

  const updateAmount = (e) => {
    const updatedAmount = e.target.value;
    setAmount(updatedAmount);
  };

  const updateCategory = (e) => {
    const updatedCategory = e.target.value;
    setCategory(updatedCategory);
  };

  const updateDifficulty = (e) => {
    const updatedDifficulty = e.target.value;
    setDifficulty(updatedDifficulty);
  };

  const getQuestions = (e) => {
    e.preventDefault();
    settingsSaved(amount, category, difficulty);
  };

  return (
    <form id="container" onSubmit={getQuestions}>
      <img src="img.png" width="15%" alt="IMG Logo" />
      <label htmlFor="amount">Number of questions:</label>
      <input
        type="text"
        name="amount"
        id="amount"
        placeholder="Number of Questions"
        value={amount}
        onChange={updateAmount}
      />
      <label htmlFor="category">Category:</label>
      <select
        id="category"
        name="category"
        value={category}
        onChange={updateCategory}
      >
        <option value="9">General</option>
        <option value="21">Sports</option>
        <option value="11">Entertainment: Film</option>
        <option value="12">Entertainment: Music</option>
        <option value="14">Entertainment: Television</option>
        <option value="17">Science & Nature</option>
        <option value="23">History</option>
        <option value="26">Celebrities</option>
        <option value="27">Animals</option>
        <option value="22">Geography</option>
        <option value="10">Mythology</option>
        <option value="24">Politics</option>
        <option value="31">Anime & Manga</option>
      </select>
      <label htmlFor="difficulty">Difficulty:</label>
      <select
        id="difficulty"
        name="difficulty"
        value={difficulty}
        onChange={updateDifficulty}
      >
        <option value="easy">Easy</option>
        <option value="medium">Medium</option>
        <option value="hard">Hard</option>
      </select>
      <button type="submit" className="btn" disabled={!amount}>
        Submit
      </button>
    </form>
  );
}
