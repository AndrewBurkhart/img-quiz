import React from 'react';
import { Link } from 'react-router-dom';

export default function FinalScore({ team1Score, team2Score }) {
  return (
    <>
      <div id="hud">
        <div className="hud-item">
          <p className="hud-prefix">Winner</p>
          {team1Score === team2Score && <h1>We have a tie :(</h1>}
          {team1Score > team2Score && (
            <h1 className="hud-main-text yellowTeam">Yellow Team</h1>
          )}
          {team1Score < team2Score && (
            <h1 className="hud-main-text blueTeam">Blue Team</h1>
          )}
        </div>
        <div className="hud-item yellowTeam">
          <p className="hud-prefix">Yellow Team</p>
          <h1 className="hud-main-text">{team1Score}</h1>
        </div>
        <div className="hud-item blueTeam">
          <p className="hud-prefix">Blue Team</p>
          <h1 className="hud-main-text">{team2Score}</h1>
        </div>
      </div>
      <Link to="/" className="btn">
        Go Home
      </Link>
    </>
  );
}
