import React, { useState } from 'react';
import { loadQuestions } from '../helpers/QuestionsHelper';
import CountdownTimer from './CountdownTimer';

export default function Question({
  question,
  changeQuestion,
  totalQuestions,
  questionNumber,
}) {
  const [classToApply, setClassToApply] = useState('');
  const [team1SelectedAnswer, setTeam1SelectedAnswer] = useState(-1);
  const [team2SelectedAnswer, setTeam2SelectedAnswer] = useState(-1);
  const [selectedAnswers, setSelectedAnswers] = useState([]);
  const [team1Bonus, setTeam1Bonus] = useState(0);
  const [team2Bonus, setTeam2Bonus] = useState(0);
  const [showAnswer, setShowAnswer] = useState(false);

  const remainingQuestions = totalQuestions - questionNumber;

  // const checkAnswers = (selectedAnswers) => {
  const checkAnswers = () => {
    setShowAnswer(true);
    // console.log(`team 2 answer is ${team2SelectedAnswer}`);
    // console.log(`Answer is ${question.answer}`);
    // console.log(
    //   selectedAnswers[0] === question.answer
    //     ? '10 points to Team 1'
    //     : 'Team 1 is wrong'
    // );
    // console.log(
    //   selectedAnswers[1] === question.answer
    //     ? '10 points to Team 2'
    //     : 'Team 2 is wrong'
    // );

    document.getElementById(question.answer).classList.add('correct');

    // setTeam1Bonus(selectedAnswers[0] === question.answer ? 10 : 0);
    // setTeam2Bonus(selectedAnswers[1] === question.answer ? 10 : 0);

    // const classToApply =
    //   selectedAnswers[0] === question.answer ? 'correct' : 'incorrect';
    // setClassToApply(classToApply);

    const removeCorrect = (i) => {
      // if (i > 0 && remainingQuestions !== 0) {
      if (remainingQuestions !== 0) {
        document.getElementById(i).classList.remove('correct');
      }
    };

    setTimeout(() => {
      setTeam1SelectedAnswer(-1);
      setTeam2SelectedAnswer(-1);
      setSelectedAnswers([]);
      setShowAnswer(false);
      changeQuestion(team1Bonus, team2Bonus);
      removeCorrect(question.answer);
    }, 10000);
  };

  const setAnswer = (selectedAnswer) => {
    if (team1SelectedAnswer === -1) {
      setTeam1SelectedAnswer(selectedAnswer);
      setSelectedAnswers((selectedAnswers) =>
        selectedAnswers.concat(selectedAnswer)
      );
      setTeam1Bonus(selectedAnswer === question.answer ? 10 : 0);
    } else {
      setTeam2SelectedAnswer(selectedAnswer);
      setSelectedAnswers((selectedAnswers) =>
        selectedAnswers.concat(selectedAnswer)
      );
      setTeam2Bonus(selectedAnswer === question.answer ? 10 : 0);
    }
  };
  const runCheck = () => {
    checkAnswers(selectedAnswers);
  };
  return (
    <div>
      <h2 dangerouslySetInnerHTML={{ __html: question.question }} />
      {question.answerChoices.map((choice, index) => (
        <div
          key={index}
          id={index}
          className={`choice-container ${
            team1SelectedAnswer === index ? 'team1Answer' : ''
          } ${team2SelectedAnswer === index ? 'team2Answer' : ''}`}
          onClick={() => setAnswer(index)}
        >
          <p className="choice-prefix">{index + 1}</p>
          <p
            className="choice-text"
            dangerouslySetInnerHTML={{ __html: choice }}
          />
        </div>
      ))}
      {team1SelectedAnswer === -1 && (
        <div>
          <h2 className="help-text yellowTeam">Select Yellow Team Answer</h2>
        </div>
      )}
      {team1SelectedAnswer !== -1 && team2SelectedAnswer === -1 && (
        <div>
          <h2 className="help-text blueTeam">Select Blue Team Answer</h2>
        </div>
      )}
      {team1SelectedAnswer !== -1 && team2SelectedAnswer !== -1 && (
        <div>
          <button type="button" className="btn" onClick={runCheck}>
            Check Answers
          </button>
        </div>
      )}
      {showAnswer && (
        <div>
          <h2>The answer is #{question.answer + 1}</h2>
        </div>
      )}
    </div>
  );
}
