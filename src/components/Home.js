import React from 'react';
import { Link } from 'react-router-dom';

export default function Home() {
  return (
    <>
      <div id="container">
        <img src="img.png" width="15%" alt="IMG Logo" />
      </div>
      <h1 style={{ marginTop: '25px' }}>Happy Hour Trivia</h1>
      <Link to="/game" className="btn">
        Start Game
      </Link>
    </>
  );
}
