const convertQuestionsFromApi = (rawQuestions) =>
  rawQuestions.map((loadedQuestion) => {
    const formattedQuestion = {
      question: loadedQuestion.question,
      answerChoices: [...loadedQuestion.incorrect_answers],
    };

    formattedQuestion.answer = Math.floor(Math.random() * 4);

    formattedQuestion.answerChoices.splice(
      formattedQuestion.answer,
      0,
      loadedQuestion.correct_answer
    );

    return formattedQuestion;
  });

export const loadQuestions = async (amount, category, difficulty) => {
  const url = `https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}&type=multiple`;
  try {
    console.log(url);
    const res = await fetch(url);
    const { results } = await res.json();
    return convertQuestionsFromApi(results);
  } catch (err) {
    console.error(err);
  }
};
